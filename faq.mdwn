[[!meta title="Frequently asked questions (FAQ)"]]

[Lars Wirzenius]: https://liw.fi/

**Who is doing this?** Puomi is primarily developed by [Lars Wirzenius][].

**Why not just use a mass-produced, cheap consumer router?** Lars
works from home, and runs many computers at home, and consumer routers
tend to not make that a pleasant experience. Lars prefers to configure
his equipment using configuration management (Ansible), and wants
things like local DNS for local machines, populated automatically from
DHCP leases.

**Why not use OpenWRT?** Lars has used OpenWRT, but prefers something
that runs on more readily available hardware, and more mainstream
Linux.

**Why not use pfSense?** It's a very good product, but Lars doesn't
know FreeBSD, and prefers to stay with Debian and Linux.

**Is this ready?** It's not finished, but Lars has been using it for
his own home since 2021.
